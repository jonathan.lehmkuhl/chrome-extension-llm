const path = require("path");
const webpack = require("webpack");
CopyWebpackPlugin = require("copy-webpack-plugin");
HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    background: "./src/background/background.js",
    content: "./src/content/content.js",
    popup: "./src/popup/popup.js",
  },
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
    ],
  },
  resolve: {
    fallback: {
      path: require.resolve("path-browserify"),
      os: require.resolve("os-browserify/browser"),
      crypto: require.resolve("crypto-browserify"),
      stream: require.resolve("stream-browserify"),
      buffer: require.resolve("buffer/"),
    },
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: "manifest.json", to: path.join(__dirname, "build") },
        { from: "imgs/icon.png", to: path.join(__dirname, "build") },
        { from: "src/popup/popup.html", to: path.join(__dirname, "build") },
        { from: "src/popup/popup.css", to: path.join(__dirname, "build") },
      ],
    }),
  ],
  devtool: "cheap-module-source-map",
};
