console.log("content.js loaded...");

let textInputNode = null;

// Uses a heuristic to select the dom element most likely to be the text input
function selectPreferredInputElement() {
  // div[contenteditable='true'] has priority one
  const divElements = document.querySelectorAll("div[contenteditable='true']");
  if (divElements.length > 0) return divElements[0];

  // textarea has priority two
  const textareaElements = document.querySelectorAll("textarea");
  if (textareaElements.length > 0) return textareaElements[0];

  // input[type='text'] has priority three
  const inputElements = document.querySelectorAll("input[type='text']");
  if (inputElements.length > 0) return inputElements[0];
}

// Returns content string of all text inputs on the page
function getTextInputContent() {
  // Initialize content string
  let textContent = "";

  // Traverses all nodes in the DOM tree of node and concatenates all text content
  function traverseNodesAndExtractText(node) {
    if (node.nodeType === Node.TEXT_NODE) textContent += node.textContent;
    else if (node.nodeType === Node.ELEMENT_NODE) {
      for (let child of node.childNodes) traverseNodesAndExtractText(child);
    }
  }

  // Find element used for text input
  textInputNode = selectPreferredInputElement();

  // Get text content of text input element
  traverseNodesAndExtractText(textInputNode);

  return textContent;
}

// Performs syntax check on text input
function handleSyntaxCheckButtonClicked() {
  // Get input text from page
  text = getTextInputContent();
  // Inform background.js to check sended text
  chrome.runtime.sendMessage({
    action: "sendTextForCheck",
    text: text,
  });
}

// Listen for incoming messages
chrome.runtime.onMessage.addListener((msg, sender, response) => {
  // Listen for syntax check button click
  if (
    msg.hasOwnProperty("action") &&
    msg.action === "notificationSyntaxCheckButtonClicked"
  ) {
    handleSyntaxCheckButtonClicked();
  }
  // Listen for corrected text from background.js
  if (msg.hasOwnProperty("action") && msg.action === "sendCorrectedText") {
    const correctedText = msg.text;
    improvementText =
      "\n% Improvement by WritingMentor: " + correctedText.trim();
    if (textInputNode.tagName.toLowerCase() === "textarea") {
      textInputNode.value += improvementText;
    }
    if (textInputNode.tagName.toLowerCase() === "div")
      textInputNode.appendChild(document.createTextNode(improvementText));
  }
});

// When not performing check on button press but continuously checking for changes, we have to use MutationObserver
// mutationObserver.observe(document.body, { childList: true, subtree: true })
