const { OpenAI } = require("langchain/llms/openai");
import { PromptTemplate } from "langchain/prompts";

console.log("background.js loaded...");

// Create LLM instance
const llm = new OpenAI({
  // APIKey as plain text, no ideal
  openAIApiKey: "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",
  // Temperature of 0 to get the most likely result and make it deterministic
  temperature: 0,
});

// Check inputText with LLM and return corrected text
async function checkWithLLM(inputText) {
  // Create prompt for syntax check with instruction-tuned LLM
  const prompt = PromptTemplate.fromTemplate(
    `Please detect the natural language text in the following and give me an improved version of it in the language of origin,
    correcting errors in grammar, spelling, and punctuation, 
    but otherwise keeping everything the same word for word. {userInput}`
  );
  const formattedPrompt = await prompt.format({ userInput: inputText });
  // Call LLM with prompt
  const res = await llm.predict(formattedPrompt);

  return res;
}

const prompt = PromptTemplate.fromTemplate(
  "What is a good name for a company that makes {product}?"
);

// Listen for incoming messages
chrome.runtime.onMessage.addListener(async (msg, sender, response) => {
  // Listen for text to check
  if (msg.hasOwnProperty("action") && msg.action === "sendTextForCheck") {
    const text = msg.text;
    // Check text with LLM
    const res = await checkWithLLM(text);
    // Inform content.js to show sended corrected text
    chrome.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
      chrome.tabs.sendMessage(tabs[0].id, {
        action: "sendCorrectedText",
        text: res,
      });
    });
  }
});
