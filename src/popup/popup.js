console.log("popup.js loaded...");

// Syntax check for written text in current tab
function syntaxCheck() {
  // Inform content.js to send text for syntax check
  chrome.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
    chrome.tabs.sendMessage(tabs[0].id, {
      action: "notificationSyntaxCheckButtonClicked",
    });
  });
}

// Called when popup.html is loaded
function main() {
  // Event handler for button
  document
    .querySelector("button#copy-button")
    .addEventListener("click", syntaxCheck);
}

document.addEventListener("DOMContentLoaded", main);
